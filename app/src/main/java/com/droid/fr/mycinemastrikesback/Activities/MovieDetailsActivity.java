package com.droid.fr.mycinemastrikesback.Activities;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.droid.fr.mycinemastrikesback.Database.DBHandler;
import com.droid.fr.mycinemastrikesback.Database.ImageVoleyRequest;
import com.droid.fr.mycinemastrikesback.Objects.Film;
import com.droid.fr.mycinemastrikesback.R;

public class MovieDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);

        Integer filmId = getIntent().getIntExtra("filmId",0);

        DBHandler db = new DBHandler(this);
        db.getReadableDatabase();
        Film film = db.getFilm(filmId);

        ImageView affiche = (ImageView) findViewById(R.id.afficheFilm_details);
        TextView titre = (TextView) findViewById(R.id.titre_film_details);
        TextView description_details = (TextView) findViewById(R.id.description_details);
        TextView description_details2 = (TextView) findViewById(R.id.description_details2);
        TextView description_synopsis = (TextView) findViewById(R.id.description_synopsis);

        titre.setText(film.getTitre());
        loadImageBitmap(affiche, film.getAffiche());

        String details1 = film.getGenre() + " | " + film.getDate_sortie();
        String details2 = film.getDuree()+" min " + " | "+ film.getCategorie();
        String synopsis = film.getSynopsis();

        description_details.setText(details1);
        description_details2.setText(details2);
        description_synopsis.setText(synopsis);

    }


    public void loadImageBitmap(final ImageView affiche, String urlToDownload) {

        ImageRequest request = new ImageRequest(urlToDownload,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        affiche.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        affiche.setImageResource(R.drawable.ic_menu_camera);
                    }
                });
        ImageVoleyRequest.getInstance(this).addToRequestQueue(request);

    }
}
