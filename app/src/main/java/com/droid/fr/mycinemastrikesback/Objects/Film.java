package com.droid.fr.mycinemastrikesback.Objects;


public class Film {

    private Integer id;
    private String titre;
    private String titre_ori;
    private String affiche;
    private String web;
    private String duree;
    private String distributeur;
    private String participants;
    private String realisateur;
    private String synopsis;
    private String annee;
    private String date_sortie;
    private String info;
    private Boolean is_visible;
    private Boolean is_vente;
    private Integer genreid;
    private Integer categorieid;
    private String genre;
    private String categorie;
    private Integer releasenumber;
    private String pays;
    private String share_url;

    private String medias;
    private String videos;
//    private List<Medias> medias;
//    private List<Videos> videos;
    private Boolean is_avp;
    private Boolean is_alaune;
    private Boolean is_lastWeek;

    public Film(){

    }

    public Film(Integer id, String titre, String titre_ori, String affiche, String web,
                String duree, String distributeur, String participants, String realisateur, String synopsis,
                String annee, String date_sortie, String info, Boolean is_visible,
                Boolean is_vente, Integer genreid, Integer categorieid, String genre,
                String categorie, Integer releasenumber, String pays, String share_url,
                String medias, String videos, Boolean is_avp, Boolean is_alaune,
                Boolean is_lastWeek) {
        this.id = id;
        this.titre = titre;
        this.titre_ori = titre_ori;
        this.affiche = affiche;
        this.web = web;
        this.duree = duree;
        this.distributeur = distributeur;
        this.participants = participants;
        this.realisateur = realisateur;
        this.synopsis = synopsis;
        this.annee = annee;
        this.date_sortie = date_sortie;
        this.info = info;
        this.is_visible = is_visible;
        this.is_vente = is_vente;
        this.genreid = genreid;
        this.categorieid = categorieid;
        this.genre = genre;
        this.categorie = categorie;
        this.releasenumber = releasenumber;
        this.pays = pays;
        this.share_url = share_url;
        this.medias = medias;
        this.videos = videos;
        this.is_avp = is_avp;
        this.is_alaune = is_alaune;
        this.is_lastWeek = is_lastWeek;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getTitre_ori() {
        return titre_ori;
    }

    public void setTitre_ori(String titre_ori) {
        this.titre_ori = titre_ori;
    }

    public String getAffiche() {
        return affiche;
    }

    public void setAffiche(String affiche) {
        this.affiche = affiche;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public String getDistributeur() {
        return distributeur;
    }

    public void setDistributeur(String distributeur) {
        this.distributeur = distributeur;
    }

    public String getParticipants() {
        return participants;
    }

    public void setParticipants(String participants) {
        this.participants = participants;
    }

    public String getRealisateur() {
        return realisateur;
    }

    public void setRealisateur(String realisateur) {
        this.realisateur = realisateur;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getAnnee() {
        return annee;
    }

    public void setAnnee(String annee) {
        this.annee = annee;
    }

    public String getDate_sortie() {
        return date_sortie;
    }

    public void setDate_sortie(String date_sortie) {
        this.date_sortie = date_sortie;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Boolean getIs_visible() {
        return is_visible;
    }

    public void setIs_visible(Boolean is_visible) {
        this.is_visible = is_visible;
    }

    public Boolean getIs_vente() {
        return is_vente;
    }

    public void setIs_vente(Boolean is_vente) {
        this.is_vente = is_vente;
    }

    public Integer getGenreid() {
        return genreid;
    }

    public void setGenreid(Integer genreid) {
        this.genreid = genreid;
    }

    public Integer getCategorieid() {
        return categorieid;
    }

    public void setCategorieid(Integer categorieid) {
        this.categorieid = categorieid;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public Integer getReleasenumber() {
        return releasenumber;
    }

    public void setReleasenumber(Integer releasenumber) {
        this.releasenumber = releasenumber;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getShare_url() {
        return share_url;
    }

    public void setShare_url(String share_url) {
        this.share_url = share_url;
    }

    public String getMedias() {
        return medias;
    }

    public void setMedias(String medias) {
        this.medias = medias;
    }

    public String getVideos() {
        return videos;
    }

    public void setVideos(String videos) {
        this.videos = videos;
    }
/*    public List<Medias> getMedias() {
        return medias;
    }

    public void setMedias(List<Medias> medias) {
        this.medias = medias;
    }

    public List<Videos> getVideos() {
        return videos;
    }

    public void setVideos(List<Videos> videos) {
        this.videos = videos;
    }
*/
    public Boolean getIs_avp() {
        return is_avp;
    }

    public void setIs_avp(Boolean is_avp) {
        this.is_avp = is_avp;
    }

    public Boolean getIs_alaune() {
        return is_alaune;
    }

    public void setIs_alaune(Boolean is_alaune) {
        this.is_alaune = is_alaune;
    }

    public Boolean getIs_lastWeek() {
        return is_lastWeek;
    }

    public void setIs_lastWeek(Boolean is_lastWeek) {
        this.is_lastWeek = is_lastWeek;
    }

    @Override
    public String toString() {
        return "Film{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", titre_ori='" + titre_ori + '\'' +
                ", affiche='" + affiche + '\'' +
                ", web='" + web + '\'' +
                ", duree=" + duree +
                ", distributeur='" + distributeur + '\'' +
                ", participants='" + participants + '\'' +
                ", realisateur='" + realisateur + '\'' +
                ", synopsis='" + synopsis + '\'' +
                ", annee=" + annee +
                ", date_sortie='" + date_sortie + '\'' +
                ", info='" + info + '\'' +
                ", is_visible=" + is_visible +
                ", is_vente=" + is_vente +
                ", genreid=" + genreid +
                ", categorieid=" + categorieid +
                ", genre='" + genre + '\'' +
                ", categorie='" + categorie + '\'' +
                ", releasenumber=" + releasenumber +
                ", pays='" + pays + '\'' +
                ", share_url='" + share_url + '\'' +
                ", medias=" + medias +
                ", videos=" + videos +
                ", is_avp=" + is_avp +
                ", is_alaune=" + is_alaune +
                ", is_lastWeek=" + is_lastWeek +
                '}';
    }


/*
    private class Medias {
        private String path;

        public Medias( String path) {
            this.path = path;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        @Override
        public String toString() {
            return "Medias{" +
                    "path='" + path + '\'' +
                    '}';
        }
    }

    private class Videos {
        private String titre;
        private Integer type;
        private String url;

        public Videos(String titre, Integer type, String url) {
            this.titre = titre;
            this.type = type;
            this.url = url;
        }

        public String getTitre() {
            return titre;
        }

        public void setTitre(String titre) {
            this.titre = titre;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public String toString() {
            return "Videos{" +
                    "titre='" + titre + '\'' +
                    ", type=" + type +
                    ", url='" + url + '\'' +
                    '}';
        }
    }
    */
}
