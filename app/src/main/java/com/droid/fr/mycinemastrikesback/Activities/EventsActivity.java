package com.droid.fr.mycinemastrikesback.Activities;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.droid.fr.mycinemastrikesback.Database.DBHandler;
import com.droid.fr.mycinemastrikesback.Objects.Film;
import com.droid.fr.mycinemastrikesback.Objects.FilmAdapter;
import com.droid.fr.mycinemastrikesback.R;

import java.util.List;

public class EventsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        //permet de faire fonctionner la barre du drawer
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Liste affichée dans l'activité
        RecyclerView recyclerViewFilm = (RecyclerView) findViewById(R.id.listView);
        recyclerViewFilm.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewFilm.setLayoutManager(layoutManager);
        recyclerViewFilm.setItemAnimator(new DefaultItemAnimator());

        DBHandler eventDB = new DBHandler(this);

        eventDB.getReadableDatabase();

        List<Film> filmList = eventDB.getAllFilm();

        FilmAdapter adapter = new FilmAdapter(EventsActivity.this, filmList);
        recyclerViewFilm.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        //Différentes action pour le drawer
        if (id == R.id.menu_alaffiche) {
            Intent intent = new Intent(EventsActivity.this,MainActivity.class);
            startActivity(intent);
            this.finish();
        } else if (id == R.id.menu_evenements) {
            Intent intent = new Intent(EventsActivity.this,EventsActivity.class);
            startActivity(intent);
            this.finish();
        } else if (id == R.id.menu_prochainement) {
            Intent intent = new Intent(EventsActivity.this,NextActivity.class);
            startActivity(intent);
            this.finish();
        } else if (id == R.id.settings_preferences) {
            Intent intent = new Intent(EventsActivity.this,PreferencesActivity.class);
            startActivity(intent);
            this.finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
