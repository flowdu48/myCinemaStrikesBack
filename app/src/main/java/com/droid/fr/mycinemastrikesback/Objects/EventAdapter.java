package com.droid.fr.mycinemastrikesback.Objects;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.droid.fr.mycinemastrikesback.R;

import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    private List<Event> events;
    private Context context;

    public EventAdapter(Context context, List<Event> events){
        this.context = context;
        this.events = events;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.event_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    //on va afficher le litre ainsi que le sous-titre d'un évenement

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Event event = events.get(position);
        holder.titre.setText(event.getTitre());
        String sousTitre = "Sous-titre: " + event.getSoustitre();
        holder.sousTitre.setText(sousTitre);
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView titre;
        public TextView sousTitre;

        public ViewHolder(View view) {
            super(view);
            titre = (TextView) view.findViewById(R.id.titre_event);
            sousTitre = (TextView) view.findViewById(R.id.soustitre);
        }
    }





}
