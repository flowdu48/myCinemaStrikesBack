package com.droid.fr.mycinemastrikesback.Objects;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.droid.fr.mycinemastrikesback.Activities.MovieDetailsActivity;
import com.droid.fr.mycinemastrikesback.Database.ImageVoleyRequest;
import com.droid.fr.mycinemastrikesback.R;

import java.util.List;

public class FilmAdapter extends RecyclerView.Adapter<FilmAdapter.ViewHolder> {

    private List<Film> films;
    private Context context;
    private ImageLoader mImageLoader;


    public FilmAdapter(Context context, List<Film> films){
        this.context = context;
        this.films = films;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.film_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    //On va afficher le titre, date de sortie et réalistateur d'un film
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Film film = films.get(position);
        holder.titre.setText(film.getTitre());
        String description = film.getGenre()+" - " + film.getDate_sortie();
        holder.description.setText(description);

        String afficheURL = film.getAffiche();
        loadImageBitmap(holder, afficheURL);

        holder.titre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MovieDetailsActivity.class).putExtra("filmId", film.getId());
                context.startActivity(intent);
            }
        });

        holder.afficheFilm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MovieDetailsActivity.class).putExtra("filmId", film.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return films.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView titre;
        public TextView description;
        public ImageView afficheFilm;

        public ViewHolder(View view) {
            super(view);
            titre = (TextView) view.findViewById(R.id.titre_film);
            description = (TextView) view.findViewById(R.id.description);
            afficheFilm = (ImageView) view.findViewById(R.id.afficheFilm);

        }
    }

    public void loadImageBitmap(final ViewHolder holder, String imageURL) {

        ImageRequest request = new ImageRequest(imageURL,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        holder.afficheFilm.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        holder.afficheFilm.setImageResource(R.drawable.ic_menu_camera);
                    }
                });
        ImageVoleyRequest.getInstance(context).addToRequestQueue(request);

    }





}
