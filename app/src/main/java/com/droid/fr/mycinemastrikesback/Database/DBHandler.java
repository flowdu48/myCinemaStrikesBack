package com.droid.fr.mycinemastrikesback.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.droid.fr.mycinemastrikesback.Objects.Event;
import com.droid.fr.mycinemastrikesback.Objects.Film;
import com.droid.fr.mycinemastrikesback.Objects.Seance;

import java.util.ArrayList;
import java.util.List;

public class DBHandler extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "FilmDatabase.db";
// All variables for Film.class
    private static final String TABLE_NAME_FILM = "film_table";
    private static final String KEY_ID_FILM = "id";
    private static final String KEY_TITRE_FILM = "titre";
    private static final String KEY_TITRE_ORI = "titre_ori";
    private static final String KEY_AFFICHE_FILM = "affiche";
    private static final String KEY_WEB = "web";
    private static final String KEY_DUREE = "duree";
    private static final String KEY_DISTRIBUTEUR = "distributeur";
    private static final String KEY_PARTICIPANTS = "participants";
    private static final String KEY_REALISATEUR = "realisateur";
    private static final String KEY_SYNOPSIS = "synopsis";
    private static final String KEY_ANNEE = "annee";
    private static final String KEY_DATE_SORTIE = "date_sortie";
    private static final String KEY_INFO = "info";
    private static final String KEY_IS_VISIBLE = "is_visible";
    private static final String KEY_IS_VENTE = "is_vente";
    private static final String KEY_GENREID = "genreid";
    private static final String KEY_CATEGORIEID_FILM = "categorieid";
    private static final String KEY_GENRE = "genre";
    private static final String KEY_CATEGORIE = "categorie";
    private static final String KEY_RELEASENUMBER = "ReleaseNumber";
    private static final String KEY_PAYS = "pays";
    private static final String KEY_SHARE_URL = "share_url";
    private static final String KEY_MEDIAS = "medias";
    private static final String KEY_VIDEOS = "videoes";
    private static final String KEY_IS_AVP = "is_avp";
    private static final String KEY_IS_ALAUNE = "is_alaune";
    private static final String KEY_IS_LASTWEEK = "is_lastWeek";

// All variables for Seance.class
    public static final String TABLE_NAME_SEANCE = "seances";
    public static final String KEY_ID_SEANCE = "id";
    public static final String KEY_ACTUAL_DATE = "actual_date";
    public static final String KEY_SHOW_TIME = "show_time";
    public static final String KEY_IS_TROISD = "is_troisd";
    public static final String KEY_IS_MALENTENDANT = "is_malentendant";
    public static final String KEY_IS_HANDICAPE = "is_handicape";
    public static final String KEY_NATIONALITY = "nationality";
    public static final String KEY_CINEMAID = "cinemaid";
    public static final String KEY_FILMID = "filmid";
    public static final String KEY_TITRE_SEANCE = "titre";
    public static final String KEY_CATEGORIEID_SEANCE = "categorieid";
    public static final String KEY_PERFORMANCEID = "performanceid";
    public static final String KEY_CINEMA_SALLE = "cinema_salle";
// All variables for Event.class
    public static final String TABLE_NAME_EVENTS = "events";
    public static final String KEY_ID_EVENTS = "id";
    public static final String KEY_TITRE_EVENTS = "titre";
    public static final String KEY_SOUSTITRE = "soustitre";
    public static final String KEY_AFFICHE_EVENTS = "affiche";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_VAD_CONDITION = "vad_condition";
    public static final String KEY_PARTENAIRE = "partenaire";
    public static final String KEY_DATE_DEB = "date_deb";
    public static final String KEY_DATE_FIN = "date_fin";
    public static final String KEY_HEURE = "heure";
    public static final String KEY_CONTACT = "contact";
    public static final String KEY_WEB_LABEL = "web_label";
    public static final String KEY_EVENEMENTTYPEID = "evenementtypeid";
    public static final String KEY_FILMS = "films";
    public static final String KEY_TYPE_WRAPPED = "type";
    public static final String KEY_TITRE_WRAPPED = "titre_wrapped";

    public DBHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    //ajout des films à la BDD
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE " + TABLE_NAME_FILM + "(" +
                        KEY_ID_FILM + " INTEGER PRIMARY KEY, " +
                        KEY_TITRE_FILM + " TEXT, " +
                        KEY_TITRE_ORI + " TEXT, " +
                        KEY_AFFICHE_FILM + " TEXT, " +
                        KEY_WEB + " TEXT, " +
                        KEY_DUREE + " INTEGER, " +
                        KEY_DISTRIBUTEUR + " TEXT, " +
                        KEY_PARTICIPANTS + " TEXT, " +
                        KEY_REALISATEUR + " TEXT, " +
                        KEY_SYNOPSIS + " TEXT, " +
                        KEY_ANNEE + " INTEGER, " +
                        KEY_DATE_SORTIE + " TEXT, " +
                        KEY_INFO + " TEXT, " +
                        KEY_IS_VISIBLE + " BOOLEAN, " +
                        KEY_IS_VENTE + " BOOLEAN, " +
                        KEY_GENREID + " INTEGER, " +
                        KEY_CATEGORIEID_FILM + " INTEGER, " +
                        KEY_GENRE + " TEXT, " +
                        KEY_CATEGORIE + " TEXT, " +
                        KEY_RELEASENUMBER + " TEXT, " +
                        KEY_PAYS + " TEXT, " +
                        KEY_SHARE_URL + " TEXT, " +
                        KEY_MEDIAS + " TEXT, " +
                        KEY_VIDEOS + " TEXT, " +
                        KEY_IS_AVP + " BOOLEAN, " +
                        KEY_IS_ALAUNE + " BOOLEAN, " +
                        KEY_IS_LASTWEEK + " BOOLEAN " +
                        ")"
        );

        //ajout des seances à la BDD
        db.execSQL(
                "CREATE TABLE "+TABLE_NAME_SEANCE+"(" +
                        KEY_ID_SEANCE + " INTEGER PRIMARY KEY, "+
                        KEY_ACTUAL_DATE+ " TEXT, " +
                        KEY_SHOW_TIME+ " TEXT, "+
                        KEY_IS_TROISD+ " BOOLEAN, "+
                        KEY_IS_MALENTENDANT+ " BOOLEAN, "+
                        KEY_IS_HANDICAPE+ " BOOLEAN, "+
                        KEY_NATIONALITY+ " TEXT, "+
                        KEY_CINEMAID+ " INTEGER, "+
                        KEY_FILMID+ " INTEGER, "+
                        KEY_TITRE_SEANCE+ " TEXT, "+
                        KEY_CATEGORIEID_SEANCE+ " INTEGER, "+
                        KEY_PERFORMANCEID+ " INTEGER, "+
                        KEY_CINEMA_SALLE+ " TEXT "+
                        ")"
        );

        //ajout des evenements à la BDD
        db.execSQL(
                "CREATE TABLE " + TABLE_NAME_EVENTS + "(" +
                        KEY_ID_EVENTS + " INTEGER PRIMARY KEY, " +
                        KEY_TITRE_EVENTS + " TEXT, " +
                        KEY_SOUSTITRE + " TEXT, " +
                        KEY_AFFICHE_EVENTS + " TEXT, " +
                        KEY_DESCRIPTION + " TEXT, " +
                        KEY_VAD_CONDITION + " TEXT, " +
                        KEY_PARTENAIRE + " TEXT, " +
                        KEY_DATE_DEB + " TEXT, " +
                        KEY_DATE_FIN + " TEXT, " +
                        KEY_HEURE + " TEXT, " +
                        KEY_CONTACT + " TEXT, " +
                        KEY_WEB_LABEL + " TEXT, " +
                        KEY_EVENEMENTTYPEID + " TEXT, " +
                        KEY_FILMS + " TEXT, " +
                        KEY_TYPE_WRAPPED + " TEXT, " +
                        KEY_TITRE_WRAPPED + " TEXT " +
                        ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME_FILM);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_SEANCE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_EVENTS);
        onCreate(db);
    }

    public void addFilm(Film film) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_ID_FILM, film.getId());
            contentValues.put(KEY_TITRE_FILM, film.getTitre());
            contentValues.put(KEY_TITRE_ORI, film.getTitre_ori());
            contentValues.put(KEY_AFFICHE_FILM, film.getAffiche());
            contentValues.put(KEY_WEB, film.getWeb());
            contentValues.put(KEY_DUREE, film.getDuree());
            contentValues.put(KEY_DISTRIBUTEUR, film.getDistributeur());
            contentValues.put(KEY_PARTICIPANTS, film.getParticipants());
            contentValues.put(KEY_REALISATEUR, film.getRealisateur());
            contentValues.put(KEY_SYNOPSIS, film.getSynopsis());
            contentValues.put(KEY_ANNEE, film.getAnnee());
            contentValues.put(KEY_DATE_SORTIE, film.getDate_sortie());
            contentValues.put(KEY_INFO, film.getInfo());
            contentValues.put(KEY_IS_VISIBLE, film.getIs_visible());
            contentValues.put(KEY_IS_VENTE, film.getIs_vente());
            contentValues.put(KEY_GENREID, film.getGenreid());
            contentValues.put(KEY_CATEGORIEID_FILM, film.getCategorieid());
            contentValues.put(KEY_GENRE, film.getGenre());
            contentValues.put(KEY_CATEGORIE, film.getCategorie());
            contentValues.put(KEY_RELEASENUMBER, film.getReleasenumber());
            contentValues.put(KEY_PAYS, film.getPays());
            contentValues.put(KEY_SHARE_URL, film.getShare_url());
            contentValues.put(KEY_MEDIAS, String.valueOf(film.getMedias()));
            contentValues.put(KEY_VIDEOS, String.valueOf(film.getVideos()));
            contentValues.put(KEY_IS_AVP, film.getIs_avp());
            contentValues.put(KEY_IS_ALAUNE, film.getIs_alaune());
            contentValues.put(KEY_IS_LASTWEEK, film.getIs_lastWeek());
            db.insert(TABLE_NAME_FILM, null, contentValues);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addSeance(Seance seance){
        SQLiteDatabase db = this.getWritableDatabase();
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_ID_SEANCE,seance.getId());
            contentValues.put(KEY_ACTUAL_DATE,seance.getActual_date());
            contentValues.put(KEY_SHOW_TIME,seance.getShow_time());
            contentValues.put(KEY_IS_TROISD,seance.getIs_troisd());
            contentValues.put(KEY_IS_MALENTENDANT,seance.getIs_malentendant());
            contentValues.put(KEY_IS_HANDICAPE,seance.getIs_handicape());
            contentValues.put(KEY_NATIONALITY,seance.getNationality());
            contentValues.put(KEY_CINEMAID,seance.getCinemaid());
            contentValues.put(KEY_FILMID,seance.getFilmid());
            contentValues.put(KEY_TITRE_SEANCE,seance.getTitre());
            contentValues.put(KEY_CATEGORIEID_SEANCE,seance.getCategorieid());
            contentValues.put(KEY_PERFORMANCEID,seance.getPerformanceid());
            contentValues.put(KEY_CINEMA_SALLE,seance.getCinema_salle());
            db.insert(TABLE_NAME_SEANCE, null, contentValues);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }


    public void addEvent(Event event){
        SQLiteDatabase db = this.getWritableDatabase();
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_TITRE_EVENTS,event.getTitre());
            contentValues.put(KEY_SOUSTITRE,event.getSoustitre());
            contentValues.put(KEY_AFFICHE_EVENTS,event.getAffiche());
            contentValues.put(KEY_DESCRIPTION,event.getDescription());
            contentValues.put(KEY_VAD_CONDITION,event.getVad_condition());
            contentValues.put(KEY_PARTENAIRE,event.getPartenaire());
            contentValues.put(KEY_DATE_DEB,event.getDate_deb());
            contentValues.put(KEY_DATE_FIN,event.getDate_fin());
            contentValues.put(KEY_HEURE,event.getHeure());
            contentValues.put(KEY_CONTACT,event.getContact());
            contentValues.put(KEY_WEB_LABEL,event.getWeb_label());
            contentValues.put(KEY_EVENEMENTTYPEID,event.getEvenementtypeid());
            //TODO CHECK IF OK WITH STRING VALUE OF
            contentValues.put(KEY_FILMS, String.valueOf(event.getFilms()));
            contentValues.put(KEY_TYPE_WRAPPED,event.getType_wrapped());
            contentValues.put(KEY_TITRE_WRAPPED,event.getTitre_wrapped());
            db.insert(TABLE_NAME_EVENTS, null, contentValues);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    // permet de renvoyer une liste de tous les films de la BDD
    public List<Film> getAllFilm() {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Film> filmListToGet = new ArrayList<Film>();
        String selectQuery = "SELECT * FROM " + TABLE_NAME_FILM;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            do {
                Film filmList = new Film();
                filmList.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_ID_FILM))));
                filmList.setTitre(cursor.getString(cursor.getColumnIndex(KEY_TITRE_FILM)));
                filmList.setTitre_ori(cursor.getString(cursor.getColumnIndex(KEY_TITRE_ORI)));
                filmList.setAffiche(cursor.getString(cursor.getColumnIndex(KEY_AFFICHE_FILM)));
                filmList.setWeb(cursor.getString(cursor.getColumnIndex(KEY_WEB)));
                filmList.setDuree(cursor.getString(cursor.getColumnIndex(KEY_DUREE)));
                filmList.setDistributeur(cursor.getString(cursor.getColumnIndex(KEY_DISTRIBUTEUR)));
                filmList.setParticipants(cursor.getString(cursor.getColumnIndex(KEY_PARTICIPANTS)));
                filmList.setRealisateur(cursor.getString(cursor.getColumnIndex(KEY_REALISATEUR)));
                filmList.setSynopsis(cursor.getString(cursor.getColumnIndex(KEY_SYNOPSIS)));
                filmList.setAnnee(cursor.getString(cursor.getColumnIndex(KEY_ANNEE)));
                filmList.setDate_sortie(cursor.getString(cursor.getColumnIndex(KEY_DATE_SORTIE)));
                filmList.setInfo(cursor.getString(cursor.getColumnIndex(KEY_INFO)));
                filmList.setIs_visible(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(KEY_IS_VISIBLE))));
                filmList.setIs_vente(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(KEY_IS_VENTE))));
                filmList.setGenreid(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_GENREID))));
                filmList.setCategorieid(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_CATEGORIEID_FILM))));
                filmList.setGenre(cursor.getString(cursor.getColumnIndex(KEY_GENRE)));
                filmList.setCategorie(cursor.getString(cursor.getColumnIndex(KEY_CATEGORIE)));
                filmList.setReleasenumber(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_RELEASENUMBER))));
                filmList.setPays(cursor.getString(cursor.getColumnIndex(KEY_PAYS)));
                filmList.setShare_url(cursor.getString(cursor.getColumnIndex(KEY_SHARE_URL)));
                filmList.setMedias(cursor.getString(cursor.getColumnIndex(KEY_MEDIAS)));
                filmList.setVideos(cursor.getString(cursor.getColumnIndex(KEY_VIDEOS)));
                filmList.setIs_avp(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(KEY_IS_AVP))));
                filmList.setIs_alaune(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(KEY_IS_ALAUNE))));
                filmList.setIs_lastWeek(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(KEY_IS_LASTWEEK))));

                filmListToGet.add(filmList);
            } while (cursor.moveToNext());
        }
        return filmListToGet;
        }
    public Film getFilm(Integer id){
        Film filmToGet = new Film();
        String selectQuery = "SELECT * FROM " + TABLE_NAME_FILM + " WHERE id = " + id  + "";


        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();

        filmToGet.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_ID_FILM))));
        filmToGet.setTitre(cursor.getString(cursor.getColumnIndex(KEY_TITRE_FILM)));
        filmToGet.setTitre_ori(cursor.getString(cursor.getColumnIndex(KEY_TITRE_ORI)));
        filmToGet.setAffiche(cursor.getString(cursor.getColumnIndex(KEY_AFFICHE_FILM)));
        filmToGet.setWeb(cursor.getString(cursor.getColumnIndex(KEY_WEB)));
        filmToGet.setDuree(cursor.getString(cursor.getColumnIndex(KEY_DUREE)));
        filmToGet.setDistributeur(cursor.getString(cursor.getColumnIndex(KEY_DISTRIBUTEUR)));
        filmToGet.setParticipants(cursor.getString(cursor.getColumnIndex(KEY_PARTICIPANTS)));
        filmToGet.setRealisateur(cursor.getString(cursor.getColumnIndex(KEY_REALISATEUR)));
        filmToGet.setSynopsis(cursor.getString(cursor.getColumnIndex(KEY_SYNOPSIS)));
        filmToGet.setAnnee(cursor.getString(cursor.getColumnIndex(KEY_ANNEE)));
        filmToGet.setDate_sortie(cursor.getString(cursor.getColumnIndex(KEY_DATE_SORTIE)));
        filmToGet.setInfo(cursor.getString(cursor.getColumnIndex(KEY_INFO)));
        filmToGet.setIs_visible(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(KEY_IS_VISIBLE))));
        filmToGet.setIs_vente(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(KEY_IS_VENTE))));
        filmToGet.setGenreid(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_GENREID))));
        filmToGet.setCategorieid(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_CATEGORIEID_FILM))));
        filmToGet.setGenre(cursor.getString(cursor.getColumnIndex(KEY_GENRE)));
        filmToGet.setCategorie(cursor.getString(cursor.getColumnIndex(KEY_CATEGORIE)));
        filmToGet.setReleasenumber(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_RELEASENUMBER))));
        filmToGet.setPays(cursor.getString(cursor.getColumnIndex(KEY_PAYS)));
        filmToGet.setShare_url(cursor.getString(cursor.getColumnIndex(KEY_SHARE_URL)));
        filmToGet.setMedias(cursor.getString(cursor.getColumnIndex(KEY_MEDIAS)));
        filmToGet.setVideos(cursor.getString(cursor.getColumnIndex(KEY_VIDEOS)));
        filmToGet.setIs_avp(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(KEY_IS_AVP))));
        filmToGet.setIs_alaune(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(KEY_IS_ALAUNE))));
        filmToGet.setIs_lastWeek(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(KEY_IS_LASTWEEK))));

        return filmToGet;
    }

    //permet de renvoyer une liste de tous les events
    public List<Event> getAllEvents(){
        SQLiteDatabase db = this.getReadableDatabase();
        List<Event> eventListToGet = new ArrayList<Event>();
        String selectQuery = "SELECT * FROM " + TABLE_NAME_EVENTS;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            do{
                Event eventList = new Event();
                eventList.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_ID_EVENTS))));
                eventList.setTitre(cursor.getString(cursor.getColumnIndex(KEY_TITRE_EVENTS)));
                eventList.setSoustitre(cursor.getString(cursor.getColumnIndex(KEY_SOUSTITRE)));
                eventList.setAffiche(cursor.getString(cursor.getColumnIndex(KEY_AFFICHE_EVENTS)));
                eventList.setDescription(cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION)));
                eventList.setVad_condition(cursor.getString(cursor.getColumnIndex(KEY_VAD_CONDITION)));
                eventList.setPartenaire(cursor.getString(cursor.getColumnIndex(KEY_PARTENAIRE)));
                eventList.setDate_deb(cursor.getString(cursor.getColumnIndex(KEY_DATE_DEB)));
                eventList.setDate_fin(cursor.getString(cursor.getColumnIndex(KEY_DATE_FIN)));
                eventList.setHeure(cursor.getString(cursor.getColumnIndex(KEY_HEURE)));
                eventList.setContact(cursor.getString(cursor.getColumnIndex(KEY_CONTACT)));
                eventList.setWeb_label(cursor.getString(cursor.getColumnIndex(KEY_WEB_LABEL)));
                eventList.setEvenementtypeid(cursor.getString(cursor.getColumnIndex(KEY_EVENEMENTTYPEID)));
                eventList.setType_wrapped(cursor.getString(cursor.getColumnIndex(KEY_TYPE_WRAPPED)));
                eventList.setTitre_wrapped(cursor.getString(cursor.getColumnIndex(KEY_TITRE_WRAPPED)));

                eventListToGet.add(eventList);
            }while(cursor.moveToNext());
        }
        return eventListToGet;
    }

        public int getFilmCount() {
        int num;
        SQLiteDatabase db = this.getReadableDatabase();
        try{
            String QUERY = "SELECT * FROM "+ TABLE_NAME_FILM;
            Cursor cursor = db.rawQuery(QUERY, null);
            num = cursor.getCount();
            db.close();
            return num;
        }catch (Exception e){
            Log.e("error",e+"");
        }
        return 0;
    }

    //vérification de la présence d'un film dans la BDD pour éviter d'ajouter deux fois le même
    public boolean isFilmInDb(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query = "SELECT * FROM " + TABLE_NAME_FILM + " WHERE " + KEY_ID_FILM + " = " + id;
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    // vérification de la présence d'une séance dans le BDD
    public boolean isSeanceInDb(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query = "SELECT * FROM " + TABLE_NAME_SEANCE + " WHERE " + KEY_ID_SEANCE + " = " + id;
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }




}
