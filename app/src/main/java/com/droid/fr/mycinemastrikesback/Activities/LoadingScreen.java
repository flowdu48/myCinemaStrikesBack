package com.droid.fr.mycinemastrikesback.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.droid.fr.mycinemastrikesback.Database.AppController;
import com.droid.fr.mycinemastrikesback.Database.DBHandler;
import com.droid.fr.mycinemastrikesback.Objects.Film;
import com.droid.fr.mycinemastrikesback.Objects.Seance;
import com.droid.fr.mycinemastrikesback.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class LoadingScreen extends AppCompatActivity {

    private String eventsURL = "http://centrale.corellis.eu/events.json";
    private String filmSeancesURL = "http://centrale.corellis.eu/filmseances.json";
    private String prochainementURL = "http://centrale.corellis.eu/prochainement.json";
    private String seancesURL = "http://centrale.corellis.eu/seances.json";

    private static String TAG = LoadingScreen.class.getSimpleName();

    private ProgressDialog progressDialog;
    private TextView txtResponse;

    // temporary string for response
    private String jsonResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading_screen);

        txtResponse = (TextView) findViewById(R.id.txtResponse);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Veuillez patienter...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        showProgressDialog();
        if(isOnline()){
            //Vérification de la présence d'un fichier de base de données et de la connexion internet
            File file = new File("/data/data/com.droid.fr.mycinemastrikesback/databases/FilmDatabase.db");
            if(!file.exists())
                makeRequest();
        }else{
            showMainActivity();
        }


        makeRequest();

    }

    private void makeProchainementRequest() {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                prochainementURL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object

                    JSONObject phone = response.getJSONObject("phone");

                    //TODO implementer la methode

                    jsonResponse = "";


                    txtResponse.setText(jsonResponse);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                hideProgressDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);

    }


    private void makeRequest() {

        final Integer[] requestCounter = {0};
        showProgressDialog();

        //Requête pour le JSON des séances
        StringRequest seanceRequest = new StringRequest(seancesURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        Log.d(TAG, String.valueOf(response));
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                parseSeance(response);
                                requestCounter[0]++;
                                if(requestCounter[0] == 3){
                                    showMainActivity();
                                    hideProgressDialog();
                                }
                            }
                        }).start();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        //Requête pour le JSON des FilmsSeances
        StringRequest filmSeanceRequest = new StringRequest(filmSeancesURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        Log.d(TAG, String.valueOf(response));
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                parseFilmSeances(response);
                                requestCounter[0]++;
                                if(requestCounter[0] == 3){
                                    showMainActivity();
                                    hideProgressDialog();
                                }

                            }
                        }).start();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


        //Requête pour le JSON des évenements
        StringRequest eventsRequest = new StringRequest(eventsURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        Log.d(TAG, String.valueOf(response));
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                parseEvents(response);
                                requestCounter[0]++;
                                if(requestCounter[0] == 3){
                                    showMainActivity();
                                    hideProgressDialog();
                                }

                            }
                        }).start();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(seanceRequest);
        requestQueue.add(filmSeanceRequest);
        requestQueue.add(eventsRequest);

    }

    //Méthode pour simplifier le code pour passer à l'activité princiapale
    private void showMainActivity() {
        hideProgressDialog();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    //méthode de parsing du JSON Seance
    private void parseSeance(String seance) {
        JSONArray array;
        JSONObject object;
        try {
            array = new JSONArray(seance);
            DBHandler myDB = new DBHandler(getApplicationContext());
            for (Integer iSeance = 0; iSeance < array.length(); iSeance++) {
                object = array.getJSONObject(iSeance);
                Seance seanceList = new Seance(object.getInt("id"),object.getString("actual_date"),object.getString("show_time"),object.getBoolean("is_troisd"),
                        object.getBoolean("is_malentendant"),object.getBoolean("is_handicape"),object.getString("nationality"),object.getInt("cinemaid"),
                        object.getInt("filmid"),object.getString("titre"),object.getInt("categorieid"),object.getInt("performanceid"),object.getString("cinema_salle"));
                if(!myDB.isSeanceInDb(seanceList.getId())){
                    Log.d("Seance", String.valueOf(seanceList));
                    myDB.addSeance(seanceList);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //méthode de parsing du JSON FilmSeances
    private void parseFilmSeances(String json){
        JSONArray array;
        JSONObject object;
        try {
            array = new JSONArray(json);
            DBHandler myDB = new DBHandler(getApplicationContext());
            for (Integer iFilmSeances = 0; iFilmSeances < array.length(); iFilmSeances++) {
                object = array.getJSONObject(iFilmSeances);
                Film filmList = new Film(object.getInt("id"),object.getString("titre"),object.getString("titre_ori"),object.getString("affiche"),object.getString("web"),
                        object.getString("duree"),object.getString("distributeur"),object.getString("participants"),object.getString("realisateur"),object.getString("synopsis"),
                        object.getString("annee"),object.getString("date_sortie"),object.getString("info"),object.getBoolean("is_visible"),object.getBoolean("is_vente"),
                        object.getInt("genreid"), object.getInt("categorieid"), object.getString("genre"),object.getString("categorie"), object.getInt("ReleaseNumber"),object.getString("pays"),
                        object.getString("share_url"),object.getString("medias"), object.getString("videos"), object.getBoolean("is_avp"), object.getBoolean("is_alaune"), object.getBoolean("is_lastWeek"));
                if(!myDB.isFilmInDb(filmList.getId())){
                    Log.d("FilmSeances", String.valueOf(filmList));
                    myDB.addFilm(filmList);
                }
            }
            }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //méthode de parsing du JSON Events
    private void parseEvents(String json){
        JSONArray array;
        JSONObject object;
        try{
            array = new JSONArray(json);
            DBHandler myDB = new DBHandler(getApplicationContext());
            for (Integer iEvents = 0; iEvents < array.length(); iEvents++) {
                object = array.getJSONObject(iEvents);
                Film filmList = new Film(object.getInt("id"),object.getString("titre"),object.getString("titre_ori"),object.getString("affiche"),object.getString("web"),
                        object.getString("duree"),object.getString("distributeur"),object.getString("participants"),object.getString("realisateur"),object.getString("synopsis"),
                        object.getString("annee"),object.getString("date_sortie"),object.getString("info"),object.getBoolean("is_visible"),object.getBoolean("is_vente"),
                        object.getInt("genreid"), object.getInt("categorieid"), object.getString("genre"),object.getString("categorie"), object.getInt("ReleaseNumber"),
                        object.getString("pays"),object.getString("share_url"),object.getString("medias"), object.getString("videos"), object.getBoolean("is_avp"),
                        object.getBoolean("is_alaune"), object.getBoolean("is_lastWeek"));
                if(!myDB.isFilmInDb(filmList.getId())){
                    Log.d("Events", String.valueOf(filmList));
                    myDB.addFilm(filmList);
                }
            }

            }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Action sur le messag de chargement
    private void showProgressDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hideProgressDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    //Online checker
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

}
